# Doom-Zscript-Examples

Copyright Kris Occhipinti 2024-04-19

(https://filmsbykris.com)

License GPLv3

# Video Tutorials
These examples go along with this videos series

[![Tutorial Playlist](https://img.youtube.com/vi/54TTvjK2PnM/0.jpg)](https://www.youtube.com/watch?v=Jm2NIAqGUoI&list=PLcUid3OP_4OVvFYYrai9LQyMl2KMjdy_M)

# Usage
To test these examples move into the folder of the example you want to test and run the following command
```bash
gzdoom -file .
```
-
# Helpful Links
- https://zdoom-docs.github.io/staging/Introduction.html

